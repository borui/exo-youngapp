# Exercice React : un jeu Questions/Réponses

## Overview

### Back-end
Le back-end se fait en 2 versions: l’un est réalisé en NodeJS sans Framework; l’autre en HapiJS, qui est un Framework basé sur NodeJS.

### Front-end
Le front-end se fait en React avec script [create-react-app](https://facebook.github.io/react/blog/2016/07/22/create-apps-with-no-configuration.html) .

## Installation

### Back-end
Aller dans le répertoire `back` puis lancer la commande
```
npm install
```

### Front-end
Aller dans le répertoire `front`puis lancer la commande
```
npm install
```

## Run

### Back-end
Pour lancer le back-end serveur, aller dans le répertoire `back` puis
```
npm run node-server
```
ou
```
npm run haps-server
```
*Attention: les deux serveur écoutent sur la même porte 4000, veuillez ne pas lancer les deux en même temps.*

### Front-end
Pour lancer l’application React
```
npm start
```
