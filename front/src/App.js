import React, { Component } from 'react';
import logo from './images/logo.png';
import './css/App.css';

class App extends Component {
    state = {};

    // Récupérer les données via API
    componentDidMount() {
        fetch('/api/v1/data')
            .then(res => {
                if (res.ok) {
                    return res.json();
                }
                throw new Error('Network response was not ok.');
            })
            .then(res => {
                this.setState({
                  ...res
                });
            })
            .catch(err => {
                console.error('Fetch operation fails');
            });
    }

    setChoice = (key) => {
        this.setState({
            choice: key
        });
    };

    render() {
        const {question, answers, choice} = this.state;

        // Si un choix est fait, y associer la class `choice-checked` 
        let leftChoiceClass = 'choice';
        let rightChoiceClass = 'choice';
        switch (choice) {
            case 'yes':
                leftChoiceClass += ' choice-checked';
                break;
            case 'no':
                rightChoiceClass += ' choice-checked';
                break;
            default:
        }

        // Si les données sont récupérées, les afficher
        const renderContent = () => {
            if (question && answers) {
                return (
                    <div className="cover">
                        <div className="question">{question}</div>
                        
                        <div>
                            <span 
                                className={leftChoiceClass}
                                onClick={() => {this.setChoice('yes')}}>Oui</span>
                            <span 
                                className={rightChoiceClass} 
                                onClick={() => {this.setChoice('no')}}>Non</span>
                        </div>
                        
                        {choice ? <div className="answer">{answers[choice]}</div> : null}
                    </div>
                )
            }
        };

        return (
            <div>
                <div className="card">
                    <div className="header">
                        <img className="logo" src={logo} alt=""></img>
                    </div>
                    <div className="body">
                        {renderContent()}
                    </div>
                </div>
            </div>
        )
    }
}

export default App;
