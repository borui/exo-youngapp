'use strict'

const Hapi = require('hapi');
const server = new Hapi.Server();

server.connection({
	host: 'localhost',
	port: 4000
});

server.route({
	method: 'GET',
	path: '/api/v1/data',
	handler: (request, reply) => {
		reply({
			question: 'Aimez-vous React ?',
			answers: {
				yes: 'Bon choix !',
				no: 'Zut !'
			}
		});
	}
});

server.start(() => {
	console.log('Hapi server started at ', server.info.uri);
});