'use strict'

const http = require('http');
const port = 4000;

const requestHandler = (req, res) => {
	console.log(`Request URL: ${req.url}`);

	if (req.url !== '/api/v1/data') {
		const errorMessage = {
			statusCode: 404,
			message: 'Not found'
		};

		res.setHeader('Content-Type', 'application/json');
		res.writeHead(404);
		return res.end(JSON.stringify(errorMessage));
	}

	const data = {
		question: 'Aimez-vous React ?',
		answers: {
			yes: 'Bon choix !',
			no: 'Zut !'
		}
	};

	res.setHeader('Content-Type', 'application/json');
	res.writeHead(200);
	res.end(JSON.stringify(data));
};

const server = http.createServer(requestHandler);

server.listen(port, (err) => {
	if (err) {
		return console.log(err);
	}

	console.log(`Node server started at port: ${port}`);
});